from scapy.all import send
from scapy.layers.inet import IP, TCP
from tkinter import Tk, Label, Entry, TOP, W, Button
from functools import partial
from config import ip

def send_packet(root):
    slaves = root.pack_slaves()
    try:
        dst, dport = slaves[1].get().split(':')
        if dst != "" and dport != "":
            dport = int(dport)

        src, sport = slaves[3].get().split(':')
        if src != "" and sport != "":
            sport = int(sport)

        data = slaves[5].get()
        getdata = True
    except:
        getdata = False

    if False:
        p=IP(dst=dst, src = src)/TCP(dport=dport, sport = sport)/data
    else:
        p=IP(dst=ip, src = None)/TCP(dport=22, sport = 22)/"mydata"

    p.show2()
    send(p)



if __name__ == "__main__":
    root = Tk()
    root.title('TCP Generator')
    root.geometry('190x180')

    Label(root, text = 'Данные назначения IP:port').pack(side = TOP, anchor = W)
    Entry(root).pack(side = TOP, anchor = W)
    Label(root, text = 'Данные источника IP:port').pack(side = TOP, anchor = W)
    Entry(root).pack(side = TOP, anchor = W)
    Label(root, text = 'Текст сообщения').pack(side = TOP, anchor = W)
    Entry(root).pack(side = TOP, anchor = W)
    Button(root, text='Отправить', command=partial(send_packet, root) ).pack( side = TOP, anchor = W)

    root.resizable(False,False)
    root.mainloop()
