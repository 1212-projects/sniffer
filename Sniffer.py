from scapy.all import sniff
import sys

options = ["ip", "icmp", "udp", "tcp", "syn"]

def sniffing(filter):
    stop = False
    while not stop:
        packets = []
        try:
            print("Процесс пошел")
            packets = sniff(filter=filter)
        except KeyboardInterrupt:
            pass
        with open("log.log", "a") as file:
            for i in packets:
                file.write(i.show2(dump=True) + "\n\n")
        print("\nПоймано пакетов: {}".format(len(packets)))
        print("Для продолжения: y/n")
        inp = input()
        stop = True if inp is 'n' else False

if not set(sys.argv[1:]).issubset(set(options)):
    print("Неверные аргументы!")
else:
    filter = " or ".join(sys.argv[1:])
    sniffing(filter)
